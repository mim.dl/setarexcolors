package ir.delnavazi.setarexcolors

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.orhanobut.hawk.Hawk

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Hawk.init(baseContext).build();

        if (Hawk.contains("nightMode")) {
            if (Hawk.get("nightMode")) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }
    }

}